<?php

namespace App\Entity;

use App\Repository\MapaEstoqueRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MapaEstoqueRepository::class)
 */
class MapaEstoque
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $mes;

    /**
     * @ORM\Column(type="integer")
     */
    private $ano;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $qtdeInicial;

    /**
     * @ORM\Column(type="integer")
     */
    private $entrada;

    /**
     * @ORM\Column(type="integer")
     */
    private $saida;

    /**
     * @ORM\Column(type="integer")
     */
    private $qtdeFinal;

    /**
     * @ORM\ManyToOne(targetEntity=MapaCalibre::class, inversedBy="mapaEstoques")
     * @ORM\JoinColumn(nullable=false)
     */
    private $calibre;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMes(): ?int
    {
        return $this->mes;
    }

    public function setMes(int $mes): self
    {
        $this->mes = $mes;

        return $this;
    }

    public function getAno(): ?int
    {
        return $this->ano;
    }

    public function setAno(int $ano): self
    {
        $this->ano = $ano;

        return $this;
    }

    public function getQtdeInicial(): ?int
    {
        return $this->qtdeInicial;
    }

    public function setQtdeInicial(?int $qtdeInicial): self
    {
        $this->qtdeInicial = $qtdeInicial;

        return $this;
    }

    public function getEntrada(): ?int
    {
        return $this->entrada;
    }

    public function setEntrada(int $entrada): self
    {
        $this->entrada = $entrada;

        return $this;
    }

    public function getSaida(): ?int
    {
        return $this->saida;
    }

    public function setSaida(int $saida): self
    {
        $this->saida = $saida;

        return $this;
    }

    public function getQtdeFinal(): ?int
    {
        return $this->qtdeFinal;
    }

    public function setQtdeFinal(int $qtdeFinal): self
    {
        $this->qtdeFinal = $qtdeFinal;

        return $this;
    }

    public function getCalibre(): ?MapaCalibre
    {
        return $this->calibre;
    }

    public function setCalibre(?MapaCalibre $calibre): self
    {
        $this->calibre = $calibre;

        return $this;
    }

    public function __toString()
    {
        return (string) $this->getId();
    }
}
