<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="parametros",indexes={
 *          @ORM\Index(name="idparametro_idx", columns={"id"})
 *      })
 */
class Parametros
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Length(max="30", min="0",maxMessage="Valor não pode ser maior que {{ limit }}")
     */
    private $printer_darkness;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPrinterDarkness()
    {
        return $this->printer_darkness;
    }

    /**
     * @param mixed $printer_darkness
     */
    public function setPrinterDarkness($printer_darkness)
    {
        $this->printer_darkness = $printer_darkness;
    }




}