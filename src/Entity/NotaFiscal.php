<?php
namespace App\Entity;

Class NotaFiscal
{

    private $id_nf;
    private $nota;
    private $fornecedor;
    private $total;
    private $data;

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total): void
    {
        $this->total = $total;
    }


    /**
     * @return mixed
     */
    public function getIdNf()
    {
        return $this->id_nf;
    }

    /**
     * @param mixed $id_nf
     */
    public function setIdNf($id_nf)
    {
        $this->id_nf = $id_nf;
    }

    /**
     * @return mixed
     */
    public function getNota()
    {
        return $this->nota;
    }

    /**
     * @param mixed $nota
     */
    public function setNota($nota)
    {
        $this->nota = $nota;
    }

    /**
     * @return mixed
     */
    public function getFornecedor()
    {
        return $this->fornecedor;
    }

    /**
     * @param mixed $fornecedor
     */
    public function setFornecedor($fornecedor)
    {
        $this->fornecedor = $fornecedor;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }



}