<?php

namespace App\Entity;

use App\Repository\MapaMovimentoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MapaMovimentoRepository::class)
 */
class MapaMovimento
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $mes;

    /**
     * @ORM\Column(type="integer")
     */
    private $ano;

    /**
     * @ORM\Column(type="integer")
     */
    private $qtde;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $registro;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $docFiscal;

    /**
     * @ORM\ManyToOne(targetEntity=MapaClienteFornecedor::class, inversedBy="mapaMovimentos",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $clienteFornecedor;

    /**
     * @ORM\ManyToOne(targetEntity=MapaProduto::class, inversedBy="mapaMovimentos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $produto;

    /**
     * @ORM\Column(type="integer")
     */
    private $docFiscalItem;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $tipoMov;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMes(): ?int
    {
        return $this->mes;
    }

    public function setMes(int $mes): self
    {
        $this->mes = $mes;

        return $this;
    }

    public function getAno(): ?int
    {
        return $this->ano;
    }

    public function setAno(int $ano): self
    {
        $this->ano = $ano;

        return $this;
    }

    public function getQtde(): ?int
    {
        return $this->qtde;
    }

    public function setQtde(int $qtde): self
    {
        $this->qtde = $qtde;

        return $this;
    }

    public function getRegistro(): ?string
    {
        return $this->registro;
    }

    public function setRegistro(string $registro): self
    {
        $this->registro = $registro;
        return $this;
    }

    public function getDocFiscal(): ?string
    {
        return $this->docFiscal;
    }

    public function setDocFiscal(string $docFiscal): self
    {
        $this->docFiscal = $docFiscal;

        return $this;
    }

    public function getClienteFornecedor(): ?MapaClienteFornecedor
    {
        return $this->clienteFornecedor;
    }

    public function setClienteFornecedor(?MapaClienteFornecedor $clienteFornecedor): self
    {
        $this->clienteFornecedor = $clienteFornecedor;

        return $this;
    }

    public function getProduto(): ?MapaProduto
    {
        return $this->produto;
    }

    public function setProduto(?MapaProduto $produto): self
    {
        $this->produto = $produto;

        return $this;
    }

    public function getDocFiscalItem(): ?int
    {
        return $this->docFiscalItem;
    }

    public function setDocFiscalItem(int $docFiscalItem): self
    {
        $this->docFiscalItem = $docFiscalItem;

        return $this;
    }

    public function getTipoMov(): ?string
    {
        return $this->tipoMov;
    }

    public function setTipoMov(string $tipoMov): self
    {
        $this->tipoMov = $tipoMov;

        return $this;
    }
}
