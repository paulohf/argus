<?php

namespace App\Entity;

use App\Repository\MapaProdutoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=MapaProdutoRepository::class)
 */
class MapaProduto
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $codigo;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $descricao;

    /**
     * @ORM\OneToMany(targetEntity=MapaMovimento::class, mappedBy="produto")
     */
    private $mapaMovimentos;

    /**
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity=MapaCalibre::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $calibre;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $tipoProd;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $fabricante;

    public function __construct()
    {
        $this->mapaMovimentos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    public function setCodigo(string $codigo): self
    {
        $this->codigo = $codigo;

        return $this;
    }

    public function getDescricao(): ?string
    {
        return $this->descricao;
    }

    public function setDescricao(string $descricao): self
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * @return Collection|MapaMovimento[]
     */
    public function getMapaMovimentos(): Collection
    {
        return $this->mapaMovimentos;
    }

    public function addMapaMovimento(MapaMovimento $mapaMovimento): self
    {
        if (!$this->mapaMovimentos->contains($mapaMovimento)) {
            $this->mapaMovimentos[] = $mapaMovimento;
            $mapaMovimento->setProduto($this);
        }

        return $this;
    }

    public function removeMapaMovimento(MapaMovimento $mapaMovimento): self
    {
        if ($this->mapaMovimentos->contains($mapaMovimento)) {
            $this->mapaMovimentos->removeElement($mapaMovimento);
            // set the owning side to null (unless already changed)
            if ($mapaMovimento->getProduto() === $this) {
                $mapaMovimento->setProduto(null);
            }
        }

        return $this;
    }

    public function getCalibre(): ?MapaCalibre
    {
        return $this->calibre;
    }

    public function setCalibre(?MapaCalibre $calibre): self
    {
        $this->calibre = $calibre;

        return $this;
    }

    public function __toString()
    {
        return (string)$this->getId();
    }

    public function getTipoProd(): ?string
    {
        return $this->tipoProd;
    }

    public function setTipoProd(string $tipoProd): self
    {
        $this->tipoProd = $tipoProd;

        return $this;
    }

    public function getFabricante(): ?string
    {
        return $this->fabricante;
    }

    public function setFabricante(?string $fabricante): self
    {
        $this->fabricante = $fabricante;

        return $this;
    }
}
