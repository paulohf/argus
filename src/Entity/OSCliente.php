<?php

namespace App\Entity;

use App\Repository\OSClienteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OSClienteRepository::class)
 */
class OSCliente
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nome;

    /**
     * @ORM\Column(type="integer")
     */
    private $cpf;

    /**
     * @ORM\Column(type="integer")
     */
    private $telefone;

    /**
     * @ORM\OneToMany(targetEntity=OSOrdem::class, mappedBy="cliente", orphanRemoval=true)
     */
    private $oSOrdems;

    public function __construct()
    {
        $this->oSOrdems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    public function getCpf(): ?int
    {
        return $this->cpf;
    }

    public function setCpf(int $cpf): self
    {
        $this->cpf = $cpf;

        return $this;
    }

    public function getTelefone(): ?int
    {
        return $this->telefone;
    }

    public function setTelefone(int $telefone): self
    {
        $this->telefone = $telefone;

        return $this;
    }

    /**
     * @return Collection<int, OSOrdem>
     */
    public function getOSOrdems(): Collection
    {
        return $this->oSOrdems;
    }

    public function addOSOrdem(OSOrdem $oSOrdem): self
    {
        if (!$this->oSOrdems->contains($oSOrdem)) {
            $this->oSOrdems[] = $oSOrdem;
            $oSOrdem->setCliente($this);
        }

        return $this;
    }

    public function removeOSOrdem(OSOrdem $oSOrdem): self
    {
        if ($this->oSOrdems->removeElement($oSOrdem)) {
            // set the owning side to null (unless already changed)
            if ($oSOrdem->getCliente() === $this) {
                $oSOrdem->setCliente(null);
            }
        }

        return $this;
    }
}
