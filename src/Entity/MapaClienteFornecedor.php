<?php

namespace App\Entity;

use App\Repository\MapaClienteFornecedorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=MapaClienteFornecedorRepository::class)
 * @UniqueEntity("cpf")
 */
class MapaClienteFornecedor
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=14,unique=true)
     */
    private $cpf;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nome;

    /**
     * @ORM\OneToMany(targetEntity=MapaMovimento::class, mappedBy="clienteFornecedor")
     */
    private $mapaMovimentos;

    /**
     * @ORM\OneToOne(targetEntity=MapaClienteFornecedorEnd::class, mappedBy="clienteFornecedor", cascade={"persist", "remove"})
     */
    private $endereco;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $telefone;

    public function __construct()
    {
        $this->mapaMovimentos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCpf(): ?string
    {
        return $this->cpf;
    }

    public function setCpf(string $cpf): self
    {
        $this->cpf = $cpf;

        return $this;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * @return Collection|MapaMovimento[]
     */
    public function getMapaMovimentos(): Collection
    {
        return $this->mapaMovimentos;
    }

    public function addMapaMovimento(MapaMovimento $mapaMovimento): self
    {
        if (!$this->mapaMovimentos->contains($mapaMovimento)) {
            $this->mapaMovimentos[] = $mapaMovimento;
            $mapaMovimento->setClienteFornecedor($this);
        }

        return $this;
    }

    public function removeMapaMovimento(MapaMovimento $mapaMovimento): self
    {
        if ($this->mapaMovimentos->contains($mapaMovimento)) {
            $this->mapaMovimentos->removeElement($mapaMovimento);
            // set the owning side to null (unless already changed)
            if ($mapaMovimento->getClienteFornecedor() === $this) {
                $mapaMovimento->setClienteFornecedor(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        return (string) $this->getId();
    }

    public function getEndereco(): ?MapaClienteFornecedorEnd
    {
        return $this->endereco;
    }

    public function setEndereco(MapaClienteFornecedorEnd $endereco): self
    {
        // set the owning side of the relation if necessary
        if ($endereco->getClienteFornecedor() !== $this) {
            $endereco->setClienteFornecedor($this);
        }

        $this->endereco = $endereco;

        return $this;
    }

    public function getTelefone(): ?string
    {
        return $this->telefone;
    }

    public function setTelefone(?string $telefone): self
    {
        $this->telefone = $telefone;

        return $this;
    }
}
