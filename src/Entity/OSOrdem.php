<?php

namespace App\Entity;

use App\Repository\OSOrdemRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OSOrdemRepository::class)
 */
class OSOrdem
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $produto;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $problema;

    /**
     * @ORM\ManyToOne(targetEntity=OSCliente::class, inversedBy="oSOrdems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cliente;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduto(): ?string
    {
        return $this->produto;
    }

    public function setProduto(string $produto): self
    {
        $this->produto = $produto;

        return $this;
    }

    public function getProblema(): ?string
    {
        return $this->problema;
    }

    public function setProblema(string $problema): self
    {
        $this->problema = $problema;

        return $this;
    }

    public function getCliente(): ?OSCliente
    {
        return $this->cliente;
    }

    public function setCliente(?OSCliente $cliente): self
    {
        $this->cliente = $cliente;

        return $this;
    }
}
