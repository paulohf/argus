<?php

namespace App\Repository;

use App\Entity\MapaCalibre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MapaCalibre|null find($id, $lockMode = null, $lockVersion = null)
 * @method MapaCalibre|null findOneBy(array $criteria, array $orderBy = null)
 * @method MapaCalibre[]    findAll()
 * @method MapaCalibre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MapaCalibreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MapaCalibre::class);
    }

    // /**
    //  * @return MapaCalibre[] Returns an array of MapaCalibre objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MapaCalibre
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
