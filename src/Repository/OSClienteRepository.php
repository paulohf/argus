<?php

namespace App\Repository;

use App\Entity\OSCliente;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OSCliente|null find($id, $lockMode = null, $lockVersion = null)
 * @method OSCliente|null findOneBy(array $criteria, array $orderBy = null)
 * @method OSCliente[]    findAll()
 * @method OSCliente[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OSClienteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OSCliente::class);
    }

    // /**
    //  * @return OSCliente[] Returns an array of OSCliente objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OSCliente
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
