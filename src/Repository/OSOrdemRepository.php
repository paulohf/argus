<?php

namespace App\Repository;

use App\Entity\OSOrdem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OSOrdem|null find($id, $lockMode = null, $lockVersion = null)
 * @method OSOrdem|null findOneBy(array $criteria, array $orderBy = null)
 * @method OSOrdem[]    findAll()
 * @method OSOrdem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OSOrdemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OSOrdem::class);
    }

    // /**
    //  * @return OSOrdem[] Returns an array of OSOrdem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OSOrdem
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
