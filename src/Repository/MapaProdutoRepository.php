<?php

namespace App\Repository;

use App\Entity\MapaProduto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MapaProduto|null find($id, $lockMode = null, $lockVersion = null)
 * @method MapaProduto|null findOneBy(array $criteria, array $orderBy = null)
 * @method MapaProduto[]    findAll()
 * @method MapaProduto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MapaProdutoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MapaProduto::class);
    }

    // /**
    //  * @return MapaProduto[] Returns an array of MapaProduto objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MapaProduto
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
