<?php

namespace App\Repository;

use App\Entity\MapaMovimentoArmas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MapaMovimentoArmas|null find($id, $lockMode = null, $lockVersion = null)
 * @method MapaMovimentoArmas|null findOneBy(array $criteria, array $orderBy = null)
 * @method MapaMovimentoArmas[]    findAll()
 * @method MapaMovimentoArmas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MapaMovimentoArmasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MapaMovimentoArmas::class);
    }

    // /**
    //  * @return MapaMovimentoArmas[] Returns an array of MapaMovimentoArmas objects
    //  */

    public function findByDocFiscalDate($dataIni, $dataFin, $entSaida)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.dataFiscal BETWEEN :dataIni and :dataFin')
            ->andWhere('m.tipoMov IN (:tipoMov)')
            ->setParameter('dataIni', $dataIni)
            ->setParameter('dataFin', $dataFin)
            ->setParameter('tipoMov', $entSaida)
            ->orderBy('m.tipoMov', 'DESC')
            ->orderBy('m.dataFiscal','ASC')
            ->getQuery()
            ->getResult();
    }

    public function findByMesAno($mes, $ano)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.ano = :ano')
            ->andWhere('m.mes = :mes')
            ->setParameter('ano', $ano)
            ->setParameter('mes', $mes)
            ->getQuery()
            ->getResult();
    }


    /*
    public function findOneBySomeField($value): ?MapaMovimentoArmas
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
