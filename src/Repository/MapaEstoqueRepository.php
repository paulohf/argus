<?php

namespace App\Repository;

use App\Entity\MapaEstoque;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MapaEstoque|null find($id, $lockMode = null, $lockVersion = null)
 * @method MapaEstoque|null findOneBy(array $criteria, array $orderBy = null)
 * @method MapaEstoque[]    findAll()
 * @method MapaEstoque[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MapaEstoqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MapaEstoque::class);
    }

    public function deleteMonthYear($mes, $ano, $tipoCalibre)
    {
        $ids = $this->createQueryBuilder('m')
            ->join('m.calibre', 'c')
            ->andWhere('m.ano >= :ano')
            ->andWhere('m.mes >= :mes')
            ->andWhere('c.tipoCalibre IN (:tipoCalibre)')
            ->setParameter('ano', $ano)
            ->setParameter('mes', $mes)
            ->setParameter('tipoCalibre', $tipoCalibre)
            ->select('m.id')
            ->getQuery()
            ->getResult();
        return $this->createQueryBuilder('m')
            ->andWhere('m.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->delete()
            ->getQuery()
            ->execute();
    }

    /**
     * @return MapaEstoque[] Returns an array of MapaEstoque objects
     */

    public function findByTipoCalibre($ano, $mes, $tipoCalibre)
    {
        return $this->createQueryBuilder('m')
            ->join('m.calibre', 'c')
            ->andWhere('c.tipoCalibre IN (:tipoCalibre)')
            ->andWhere('m.ano = :ano')
            ->andWhere('m.mes = :mes')
            ->setParameter('ano', $ano)
            ->setParameter('mes', $mes)
            ->setParameter('tipoCalibre', $tipoCalibre)
            ->orderBy('m.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


    /*
    public function findOneBySomeField($value): ?MapaEstoque
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */


}
