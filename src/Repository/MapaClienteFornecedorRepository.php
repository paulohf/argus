<?php

namespace App\Repository;

use App\Entity\MapaClienteFornecedor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MapaClienteFornecedor|null find($id, $lockMode = null, $lockVersion = null)
 * @method MapaClienteFornecedor|null findOneBy(array $criteria, array $orderBy = null)
 * @method MapaClienteFornecedor[]    findAll()
 * @method MapaClienteFornecedor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MapaClienteFornecedorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MapaClienteFornecedor::class);
    }

     /**
      * @return MapaClienteFornecedor[] Returns an array of MapaClienteFornecedor objects
      */

    public function findByName($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.nome like :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

/*

    public function findOneBySomeField($value): ?MapaClienteFornecedor
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
