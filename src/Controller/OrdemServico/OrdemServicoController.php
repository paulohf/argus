<?php

namespace App\Controller\OrdemServico;

use App\Entity\OrdemServico;
use App\Form\OrdemServicoType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class OrdemServicoController extends AbstractController
{
    private $doctrine;

    public function __construct(ManagerRegistry $doctrine)
    {

        $this->doctrine = $doctrine;
    }


    /**
     * @Route("ordemservico",name="Ordem_Servico" )
     */
    public function indexAction(Request $request)
    {
        $repository = $this->doctrine
            ->getRepository(OrdemServico::class);

        $query = $repository->createQueryBuilder('p')
            ->where('p.status = :status')
            ->setParameter('status', '0')
            ->orderBy('p.id', 'DESC')
            ->getQuery();

        $ordensServico = $query->getResult();

        return $this->render('ordem_servico/page_ordem_servico.twig', array('ordens' => $ordensServico));
    }

    /**
     * @Route("ordemservicoCRUD",name="Ordem_Servico_CRUD" )
     */
    public function crudOrdemServicoAction(Request $request)
    {

        $form = $this->createForm(OrdemServicoType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $ordemservico = $form->getData();

                $em = $this->doctrine->getManager();
                $ordemservico->setData(new \DateTime());
                $ordemservico->setStatus(0);

                $em->persist($ordemservico);
                $em->flush();


                return $this->redirectToRoute('Ordem_Servico');
            } else {
                $this->addFlash(
                    'error',
                    'Falha ao atualizar registros! ' . $form->getErrors(true)
                );
            }
        }
        return $this->render('default/ordem_servico/page_ordem_servico_maintenance.twig', array('form' => $form->createView()));
    }

    public function getDatabaseConfig()
    {

        return [
            'driver' => 'com.mysql.jdbc.Driver',
            'host' => '127.0.0.1',
            'port' => '3306',
            'username' => 'financeiro',
            'password' => 'Templar120',
            'database' => 'financeiro',
            'jdbc_dir' => $_SERVER["DOCUMENT_ROOT"] . '\..\vendor\lavela\phpjasper\bin\jasperstarter\jdbc',
        ];
    }

    /**
     * @Route("/printOS/{os}",name="PrintOS" )
     */
    public function printOS($os)
    {
        $report = new PHPJasper();
        $input = $_SERVER["DOCUMENT_ROOT"] . '\reports\OrdemServiçoTermica.jasper';
        $options = [
            'format' => ['pdf'],
            'locale' => 'en',
            'params' => ['ordem_id' => $os],
            'db_connection' => [
                'driver' => 'mysql',
                'username' => 'financeiro',
                'password' => 'Templar120',
                'host' => 'localhost',
                'database' => 'financeiro',
                'port' => '3306',
                'jdbc_url' => 'jdbc:mysql://localhost:3306/financeiro?zeroDateTimeBehavior=convertToNull',
                'jdbc_dir' => $_SERVER["DOCUMENT_ROOT"] . '\..\vendor\lavela\phpjasper\bin\jasperstarter\jdbc'

            ]
        ];
        $output = $_SERVER["DOCUMENT_ROOT"] . '\\reports\\';

        $report->process(
            $input,
            $output,
            $options
        )->execute();

        $file = $output . '\OrdemServiçoTermica.pdf';
        $path = $file;

        if (!file_exists($file)) {
            abort(404);
        }

        $pdf = new File($file);

//                unlink($path);
        return $this->file($pdf);
    }


    /**
     * @Route("/printOSNew/{osId}",name="PrintOSNew" )
     */
    public function printOSNew($osId)
    {
        $ordem = $this->doctrine
            ->getRepository(OrdemServico::class)
            ->find($osId);


        return $this->render('default/ordem_servico/orderm_servico.twig', array('ordem' => $ordem));
    }

    /**
     * @Route("/closeOS",name="Close_OS" )
     */
    public function closeOS(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $os = "";
            $os = $request->request->get('os');

            $entityManager = $this->doctrine->getManager();
            $ordemServico = $entityManager->getRepository(OrdemServico::class)->find($os);
            if ($ordemServico) {
                $ordemServico->setStatus(1);
                $entityManager->flush();
                return new Response('OK!', 200);
            }
            return new Response('Not Found!', 404);
        }
        return new Response('This is not ajax!', 400);

    }

    /**
     * @Route("/v1/os",name="os" )
     */
    public function getOsId(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $os = "";
            $os = $request->request->get('os');

            $entityManager = $this->doctrine->getManager();
            $ordemServico = $entityManager->getRepository(OrdemServico::class)->find($os);
            if ($ordemServico) {

                return new JsonResponse(array(
                    'nome' => $ordemServico->getNome(),
                    'id' => $ordemServico->getId(),
                    'data' => date_format($ordemServico->getData(), 'd/m/y'),
                    'produto' => $ordemServico->getProduto(),
                    'problema' => $ordemServico->getProblema(),
                    'urgente' => $ordemServico->getUrgente(),
                    'orcamento' => $ordemServico->getOrcamento(),
                    'telefone' => $ordemServico->getTelefone()

                ));
            }
            return new Response('Not Found!', 404);
        }
        return new Response('This is not ajax!', 400);
    }

}
