<?php


namespace App\Controller\Mapa;


use App\Entity\MapaClienteFornecedor;
use App\Entity\MapaClienteFornecedorEnd;
use App\Entity\MapaMovimento;
use App\Entity\MapaProduto;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\Routing\Annotation\Route;

class APIMapaController extends AbstractController
{
    private $caracteresEspeciais = array('-', '.', ',');
    private $doctrine;


    public function __construct(HttpClientInterface $tinyClient, ManagerRegistry $doctrine)
    {
        $this->client = $tinyClient;
        $this->doctrine = $doctrine;
    }


    /**
     * @Route ("/mapa/importar/nota_municao",name="mapa_importar_nota_municao")
     */
    public function importarNota(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $entityManager = $this->doctrine->getManager();
            $repMapaProduto = $this->doctrine->getRepository(MapaProduto::class);
            $repMapaCliente = $this->doctrine->getRepository(MapaClienteFornecedor::class);
            $repMapaMovimento = $this->doctrine->getRepository(MapaMovimento::class);

            $numeroPedidoRequest = $request->request->get('notafiscal');
            $tipoMovimento = $request->request->get('es');

            $mapaMovimento = $repMapaMovimento->findBy([
                'docFiscal' => 'P' . $numeroPedidoRequest
            ]);
            if ($mapaMovimento) {
                return new JsonResponse('Movimento já cadastrado', 200);
            }

            if ($numeroPedidoRequest <> "") {
                /*ENCONTRA O ID DO PEDIDO PELO SEU NUMERO*/
                $responsePedidos = $this->client
                    ->request('GET', '/api2/pedidos.pesquisa.php', [
                        'query' => [
                            'numero' => $numeroPedidoRequest,
                        ]
                    ]);
                $content = $responsePedidos->toArray();
                $retorno = $content['retorno'];
                if ($retorno['status'] == 'OK') {
                    $idPedido = $retorno["pedidos"][0]["pedido"]["id"];
                    /* ENCONTRA PEDIDO PELO SEU ID*/
                    $responsePedido = $this->client
                        ->request('GET', '/api2/pedido.obter.php', [
                            'query' => [
                                'id' => $idPedido,
                            ]
                        ]);
                    $conteudoNota = $responsePedido->toArray();
                    $pedidoTiny = $conteudoNota['retorno'];
                    if ($pedidoTiny['status'] == 'OK') {
                        foreach ($pedidoTiny['pedido']['itens'] as $itens) {
                            $mapaProduto = $repMapaProduto->findOneBy([
                                    'codigo' => $itens['item']['codigo']
                                ]
                            );
                            if ($mapaProduto) {
                                $mapaMovimento = new MapaMovimento();
                                $mapaMovimento->setProduto($mapaProduto);
                                $mapaMovimento->setRegistro($pedidoTiny['pedido']['obs_interna']);
                                $mapaMovimento->setDocFiscal('P' . $pedidoTiny['pedido']['numero']);
                                $mapaMovimento->setDocFiscalItem($itens['item']['id_produto']);
                                $mapaMovimento->setTipoMov('S');
                                $mapaMovimento->setQtde($itens['item']['quantidade']);
                                $dataEmissaoPedido = \DateTime::createFromFormat(
                                    'd/m/Y',
                                    $pedidoTiny['pedido']['data_faturamento']
                                );
                                $mapaMovimento->setMes($dataEmissaoPedido->format('m'));
                                $mapaMovimento->setAno($dataEmissaoPedido->format('Y'));
                                $mapaCliente = $repMapaCliente->findOneBy([
                                    'cpf' => str_replace($this->caracteresEspeciais, '', $pedidoTiny['pedido']['cliente']['cpf_cnpj'])
                                ]);
                                if ($mapaCliente) {
                                    /**
                                     * TODO: verificar se o cliente existe e se os dados estão atualizados
                                     */
                                    $mapaMovimento->setClienteFornecedor($mapaCliente);
                                } else {
                                    $mapaCliente = new MapaClienteFornecedor();
                                    $mapaCliente->setNome($pedidoTiny['pedido']['cliente']['nome']);
                                    $mapaCliente->setTelefone($pedidoTiny['pedido']['cliente']['fone']);
                                    $mapaCliente->setCpf($pedidoTiny['pedido']['cliente']['cpf_cnpj']);
                                    $mapaClienteEndereco = new MapaClienteFornecedorEnd();
                                    $mapaClienteEndereco->setClienteFornecedor($mapaCliente);
                                    $mapaClienteEndereco->setCep(
                                        str_replace($this->caracteresEspeciais, '', $pedidoTiny['pedido']['cliente']['cep'])
                                    );
                                    $mapaClienteEndereco->setBairro($pedidoTiny['pedido']['cliente']['bairro']);
                                    $mapaClienteEndereco->setCidade($pedidoTiny['pedido']['cliente']['cidade']);
                                    $mapaClienteEndereco->setUf($pedidoTiny['pedido']['cliente']['uf']);
                                    $mapaClienteEndereco->getEndereco($pedidoTiny['pedido']['cliente']['endereco']);
                                    $mapaClienteEndereco->setComplemento($pedidoTiny['pedido']['cliente']['complemento']);
                                    $mapaMovimento->setClienteFornecedor($mapaCliente);
                                }
                                $entityManager->persist($mapaMovimento);
                                $entityManager->flush();
                            }

                        }


                    }


                }
            }
            return new Response("This is not AJAX", 200);

        }

    }

    /**
     * @Route ("/mapa/pedidos/registros",name="mapa_peiddos_registros")
     */
    public function selecionaPedidosRegistro(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        $entityManager = $this->doctrine->getManager();
        $repMapaMovimento = $this->doctrine->getRepository(MapaMovimento::class);

        if ($isAjax) {
            $dtInicial = $request->request->get('dtInicial');
            $dtFinal = $request->request->get('dtFinal');
            if ($dtInicial == null) {
                return new Response('Data inicial nula', 500);
            }
            $haspage = true;
            $pedidos = array();
            $pedidosComRegistros = array();
            $pagina = 1;
            /***
             * Busca os pedidos da data inicial a final que contenham o nome do cliente diferente de
             * "consumidor Final"
             */
            while ($haspage) {
                $responsePedidos = $this->client
                    ->request('GET', '/api2/pedidos.pesquisa.php', [
                        'query' => [
                            'dataInicial' => $dtInicial,
                            'dataFinal' => $dtFinal,
                            'pagina' => $pagina
                        ]
                    ]);
                $content = $responsePedidos->toArray();
                $retorno = $content['retorno'];
                if ($retorno['status'] == 'OK') {
                    foreach ($retorno['pedidos'] as $pedido) {
                        if ($pedido['pedido']['nome'] <> 'Consumidor Final') {
                            array_push($pedidos, $pedido);
                        }
                    }
                    if ($retorno['pagina'] == $retorno['numero_paginas']) {
                        $haspage = false;
                    } else {
                        $pagina++;
                    }
                } else {
                    $haspage = false;
                }
            }
            /**
             * Procura nos pedidos recuperados as observações internas
             */
            foreach ($pedidos as $pedido) {
                $responsePedidos = $this->client
                    ->request('GET', '/api2/pedido.obter.php', [
                        'query' => [
                            'id' => $pedido['pedido']['id']
                        ]
                    ]);
                $content = $responsePedidos->toArray();
                $retorno = $content['retorno'];
                if ($retorno['status'] == 'OK') {
                    $pedido['pedido']['obs_interna'] = $retorno['pedido']['obs_interna'];
                    $mapaMovimento = $repMapaMovimento->findBy([
                        'docFiscal' => 'P' . $pedido['pedido']['numero']
                    ]);
                    if($mapaMovimento){
                        $pedido['pedido']['integracao']='<i class="fa-solid fa-download fa-lg"></i>';
                    }else{
                        $pedido['pedido']['integracao']='<i class="fa-solid fa-ban fa-lg"></i>';
                    }
                    array_push($pedidosComRegistros, $pedido);
                    sleep(1);
                }
            }
            return new JsonResponse($pedidosComRegistros);
        }
        return new Response("This is not AJAX", 403);
    }

    /**
     * @Route ("/mapa/pedidos/atualiza/registro",name="mapa_pedidos_atualiza_registro")
     */
    public function atualizaPedidosRegistro(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $idPedido = $request->request->get('id');
            $registro = $request->request->get('registro');
            $dadosPedido =
                '{
                    "dados_pedido": {
                        "obs_interna": "'. $registro .'"
                    }
                }';


            $responsePedidos = $this->client
                ->request('GET', '/api2/pedido.alterar.php', [
                    'query' => [
                        'id' => $idPedido,
                    ],
                    'body' => $dadosPedido,
                ]);
            $content = $responsePedidos->toArray();
            $retorno = $content['retorno'];
            if ($retorno['status'] == 'OK') {
                return new Response('Ok', 200);
            } else {
                return new Response('NOk', 400);
            }

        }
        return new Response('Não é ajax', 403);
    }

}