<?php

namespace App\Controller\Mapa;

use App\Entity\MapaEstoque;
use App\Entity\MapaMovimento;
use App\Entity\MapaMovimentoArmas;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Length;
use App\Entity\NotaFiscalMapa;


class MapaReportController extends AbstractController
{
    private $doctrine;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }


    /**
     * @Route("MapaReporVendasMunicao",name="MapaReporteVendasMunicao")
     *
     */
    public function mapaReporteVendasMunicaoAction(Request $request)
    {
        $reportParam = array();
        $form = $this->createFormBuilder($reportParam, array('attr' => array('target' => '_blank')))
            ->add('mes', IntegerType::class, array('constraints' => new Length(array(
                'min' => 1, 'max' => 12,
                'maxMessage' => 'Mês não pode ser maior do que {{ limit }}'
            ))))
            ->add('ano', IntegerType::class)
            ->add('data', DateType::class, array(
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'date'],
                'format' => 'dd/MM/yyyy',
            ))
            ->add('tipo', ChoiceType::class, array('choices' => array(
                'Inventário de Munições' => 'Inventario',
                'Vendas a Clientes' => 'Clientes',
            )))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                $data = $form->getData();
                // return $this->file($this->printReportMunicao($template[$data['tipo']], $data['tipo'], $data['ano'], $data['mes'], $data['data']));
                if ($data['tipo'] === 'Inventario') {
                    $tipoCalibre = array('Projétil', 'Pólvora', 'Cartucho Vazio', 'Cartucho', 'Espoleta');
                    $inventario = $this->doctrine
                        ->getRepository(MapaEstoque::class)
                        ->findByTipoCalibre(
                            $data['ano'],
                            $data['mes'],
                            $tipoCalibre
                        );
                    if ($inventario) {
                        return $this->render(
                            'mapa/relatorio_venda_municao_inventario.twig',
                            array('inventario' => $inventario,
                                'mes' => $data['mes'],
                                'ano' => $data['ano'],
                                'data' => $data['data']
                            )
                        );
                    } else {
                        $this->addFlash(
                            'error',
                            'Nenhum registro encontrado! ' . $form->getErrors(true)
                        );
                    }
                } else {
                    $movClientes = $this->doctrine
                        ->getRepository(MapaMovimento::class)
                        ->findBy(['ano' => $data['ano'],
                            'mes' => $data['mes'],
                            'tipoMov' => 'S',
                        ],
                            ['id' => 'DESC']
                        );
                    if ($movClientes) {
                        return $this->render(
                            'mapa/relatorio_venda_munição_clientes.twig',
                            array('movimentos' => $movClientes,
                                'mes' => $data['mes'],
                                'ano' => $data['ano'],
                                'data' => $data['data']
                            )
                        );
                    } else {
                        $this->addFlash(
                            'error',
                            'Nenhum registro encontrado! ' . $form->getErrors(true)
                        );
                    }
                }
            }
        }
        return $this->render('mapa/page_imprimir_mapa_venda_municao.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("MapaReportVendasArmas",name="MapaReportVendasArmas")
     */
    public function mapaReporteVendaArmas(Request $request)
    {
        $teste = array();
        $form = $this->createFormBuilder($teste, array('attr' => array('target' => '_blank')))
            ->add('mes', IntegerType::class, array('label' => 'Mês'))
            ->add('ano', IntegerType::class, array('label' => 'Ano'))
            ->add('dataRelatorio', DateType::class, array(
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd/MM/yyyy',
                'label' => 'Data Relatório'
            ))
            ->add('tipo', ChoiceType::class, array('choices' => array(
                'Inventário de Armas' => 'Inventario',
                'Vendas a Clientes' => 'Clientes',
            )))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                $data = $form->getData();

                if ($data['tipo'] == 'Clientes') {
                    $dataInicial = $data['ano'] . '-' . $data['mes'] . '-01';
                    $dataFinal = $data['ano'] . '-' . $data['mes']
                        . '-'
                        . cal_days_in_month(CAL_GREGORIAN, $data['mes'], $data['ano']);
                    $entSaida = array('S');
                    $mapaMovimentoArmas = $this->doctrine
                        ->getRepository(MapaMovimentoArmas::class)
                        ->findByDocFiscalDate(
                            $dataInicial,
                            $dataFinal,
                            $entSaida
                        );
                    if ($mapaMovimentoArmas) {
                        return $this->render(
                            'mapa/relatorio_venda_armas_clientes.twig',
                            array('movimentos' => $mapaMovimentoArmas,
                                'mes' => $data['mes'],
                                'ano' => $data['ano'],
                                'data' => $data['dataRelatorio']
                            )
                        );
                    } else {
                        $mapaMovimentoArmas = new MapaMovimentoArmas();
                        return $this->render(
                            'mapa/relatorio_venda_armas_clientes.twig',
                            array('movimentos' => null,
                                'mes' => $data['mes'],
                                'ano' => $data['ano'],
                                'data' => $data['dataRelatorio']
                            )
                        );
//                        $this->addFlash(
//                            'error',
//                            'Não foram encontrados registros ' . $form->getErrors(true)
//                        );

                    }
                } else {
                    $repMapaEstoque = $this->doctrine->getRepository(MapaEstoque::class);
                    $tipoCalibre = array('Arma');
                    $mapaEstoques = $repMapaEstoque->findByTipoCalibre(
                        $data['ano'],
                        $data['mes'],
                        $tipoCalibre
                    );
                    if ($mapaEstoques) {
                        return $this->render(
                            'mapa/relatorio_venda_armas_inventario.twig',
                            array('inventario' => $mapaEstoques,
                                'mes' => $data['mes'],
                                'ano' => $data['ano'],
                                'data' => $data['dataRelatorio']
                            )
                        );
                    } else {
                        $this->addFlash(
                            'error',
                            'Não foram encontrados registros ' . $form->getErrors(true)
                        );
                    }
                }
            } else {
                $this->addFlash(
                    'error',
                    'Falha ao imprimir registros! ' . $form->getErrors(true)
                );
            }
        }
        return $this->render('mapa/page_imprimir_mapa_venda_armas.twig', array('form' => $form->createView()));
    }


}
