<?php

namespace App\Controller\Mapa;

use App\Entity\MapaEstoque;
use App\Entity\MapaMovimento;
use App\Entity\MapaParametro;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class MapaController extends AbstractController
{
    private $doctrine;

    public function __construct(HttpClientInterface $tinyClient, ManagerRegistry $doctrine)
    {
        $this->client = $tinyClient;
        $this->doctrine = $doctrine;
    }

    /**
     * @Route("/mapa/controle/municao", name="mapa_controle_municao")
     */
    public function index()
    {
        $repository = $this->doctrine->getRepository(MapaParametro::class);
        $parametro = $repository->find(1);
        return $this->render('mapa/mapa_controle.twig', [
            'controller_name' => 'MapaController', 'parametro' => $parametro
        ]);
    }

    /**
     * @Route("/mapa/estoque/saldos", name="mapa_estoque_saldos",methods={"GET"})
     */
    public function getInventory(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $repMapaEstoque = $this->doctrine->getRepository(MapaEstoque::class);
            $repParamentro = $this->doctrine->getRepository(MapaParametro::class);
            $parametro = $repParamentro->find(1);
            $tipoCalibre = array('Projétil', 'Pólvora', 'Cartucho Vazio', 'Cartucho','Espoleta');
            $mapaEstoques = $repMapaEstoque->findByTipoCalibre(
                $parametro->getAnoAberto(),
                $parametro->getMesAberto(),
                $tipoCalibre
            );
            if ($mapaEstoques) {
                $estoqueArray = array();
                foreach ($mapaEstoques as $estoque) {
                    array_push($estoqueArray, array(
                        "id" => $estoque->getId(),
                        "calibre" => $estoque->getCalibre()->getDescricao(),
                        "tipo" => $estoque->getCalibre()->getTipoCalibre(),
                        "saldoInicial" => $estoque->getQtdeInicial(),
                        "entrada" => $estoque->getEntrada(),
                        "saida" => $estoque->getSaida(),
                        "saldoFinal" => $estoque->getQtdeFinal()
                    ));
                }
                return new JsonResponse($estoqueArray);
            }
            return new JsonResponse('not found', 404);
        }
        return new JsonResponse('Is not Ajax', 405);
    }

    /**
     * @Route("/mapa/saldos/calcular", name="mapa_saldos_calcular",methods={"GET"})
     */
    public function createInventory(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $repParametro = $this->doctrine->getRepository(MapaParametro::class);
            $repMapaEstoque = $this->doctrine->getRepository(MapaEstoque::class);
            $entityManager = $this->doctrine->getManager();
            $repMapaMovimento = $this->doctrine->getRepository(MapaMovimento::class);
            $parametro = $repParametro->find(1);
            $tipoCalibre = array('Projétil', 'Pólvora', 'Cartucho Vazio', 'Cartucho', 'Espoleta');

            if ($parametro->getMesAberto() == 1) {
                $mesUltimoSaldo = 12;
                $anoUltimoSaldo = $parametro->getAnoAberto() - 1;
            } else {
                $mesUltimoSaldo = $parametro->getMesAberto() - 1;
                $anoUltimoSaldo = $parametro->getAnoAberto();
            }
            $repMapaEstoque->deleteMonthYear(
                $parametro->getMesAberto(),
                $parametro->getAnoAberto(),
                $tipoCalibre
            );

            $ultimoEstoqueArray = $repMapaEstoque->findByTipoCalibre(
                $anoUltimoSaldo,
                $mesUltimoSaldo,
                $tipoCalibre
            );

            if (!$ultimoEstoqueArray) {
                return new JsonResponse('ultimo inventário não encontrado', 404);
            }
            foreach ($ultimoEstoqueArray as $estoque) {
                $novoEstoque = new MapaEstoque();
                $novoEstoque->setMes($parametro->getMesAberto());
                $novoEstoque->setAno($parametro->getAnoAberto());
                $novoEstoque->setCalibre($estoque->getCalibre());
                $novoEstoque->setEntrada(0);
                $novoEstoque->setQtdeFinal(0);
                $novoEstoque->setSaida(0);
                $novoEstoque->setQtdeInicial($estoque->getQtdeFinal());
                $entityManager->persist($novoEstoque);

            }
            $entityManager->flush();

            $movimentos = $repMapaMovimento->findByTipo(
                $parametro->getMesAberto(),
                $parametro->getAnoAberto(),
                'municao'
            );

            if (!$movimentos) {
//                return new JsonResponse('movimento não encontrado', 404);
                $estoquesSemMov = $repMapaEstoque->findBy([
                    'mes' => $parametro->getMesAberto(),
                    'ano' => $parametro->getAnoAberto()
                ]);
                foreach ($estoquesSemMov as $estoqueSemMov) {
                    $estoqueSemMov->setQtdeFinal($estoqueSemMov->getQtdeInicial());
                    $entityManager->persist($estoqueSemMov);
                }
                $entityManager->flush();
            } else {
                foreach ($movimentos as $mov) {
                    $est = $repMapaEstoque->findOneBy([
                        'mes' => $parametro->getMesAberto(),
                        'ano' => $parametro->getAnoAberto(),
                        'calibre' => $mov->getProduto()->getCalibre()
                    ]);
//              *****  Se encontrar estoque soma entrada/saida
                    if ($est) {
                        if ($mov->getTipoMov() == 'E') {
                            $est->setEntrada($est->getEntrada() + $mov->getQtde());
                        } else {
                            $est->setSaida($est->getSaida() + $mov->getQtde());
                        }
                        $entityManager->persist($est);

                    } else {
//               *********  Se não encontrar cria novo estoque (munição nova)
                        $novoEstoque = new MapaEstoque();
                        $novoEstoque->setQtdeInicial(0);
                        $novoEstoque->setQtdeFinal(0);
                        $novoEstoque->setSaida(0);
                        $novoEstoque->setEntrada(0);
                        if ($mov->getTipoMov() == 'E') {
                            $novoEstoque->setEntrada($mov->getQtde());
                        } else {
                            $novoEstoque->setSaida($mov->getQtde());
                        }
                        $novoEstoque->setCalibre($mov->getProduto()->getCalibre());
                        $novoEstoque->setAno($parametro->getAnoAberto());
                        $novoEstoque->setMes($parametro->getMesAberto());
                        $entityManager->persist($novoEstoque);
                        $entityManager->flush();
                    }
                    $entityManager->flush();

                    //****** Calcula Saldo final para o mes

                    $estoques = $repMapaEstoque->findByTipoCalibre(
                        $parametro->getAnoAberto(),
                        $parametro->getMesAberto(),
                        $tipoCalibre
                    );
                    if (!$estoques) {
                        return new JsonResponse('estoques não encontrado [saldo final]', 404);
                    }

                    foreach ($estoques as $est) {
                        $est->setQtdeFinal($est->getQtdeInicial() + $est->getEntrada() - $est->getSaida());
                        $entityManager->persist($est);

                    }

                    $entityManager->flush();


                }
            }


            $parametro->setDataUltProcSaldo(\DateTime::createFromFormat('Y-m-d', date('Y-m-d')));
            $entityManager->persist($parametro);
            $entityManager->flush();

            return new JsonResponse('calculated', 200);

        }
        return new JsonResponse('This is not ajax', 505);
    }

    /**
     * @Route("/mapa/fechar/mes", name="mapa_fechar_mes",methods={"GET"})
     */
    public function closeMonth(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $repParamento = $this->doctrine->getRepository(MapaParametro::class);
            $parametro = $repParamento->find(1);

            if ($parametro->getMesAberto() == 12) {
                $parametro->setMesAberto(1);
                $parametro->setAnoAberto($parametro->getAnoAberto() + 1);
            } else {
                $parametro->setMesAberto($parametro->getMesAberto() + 1);
            }

            $entitymanager = $this->doctrine->getManager();
            $entitymanager->persist($parametro);
            $entitymanager->flush();
            return new JsonResponse('ok', 200);

        }
        return new Response('This is not ajax', 505);
    }

    /**
     * @Route("/mapa/reabrir/mes", name="mapa_reabrir_mes",methods={"GET"})
     */
    public function openMonth(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $repParamento = $this->doctrine->getRepository(MapaParametro::class);
            $parametro = $repParamento->find(1);

            if ($parametro->getMesAberto() == 1) {
                $parametro->setMesAberto(12);
                $parametro->setAnoAberto($parametro->getAnoAberto() - 1);
            } else {
                $parametro->setMesAberto($parametro->getMesAberto() - 1);
            }

            $entitymanager = $this->doctrine->getManager();
            $entitymanager->persist($parametro);
            $entitymanager->flush();

            return new JsonResponse('ok', 200);

        }
        return new JsonResponse('This is not ajax', 505);
    }

    /**
     * @Route("/mapa/parametros", name="mapa_parametros",methods={"GET"})
     */

    public function getParam(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $repParametro = $this->doctrine->getRepository(MapaParametro::class);
            $parametro = $repParametro->find(1);

            $jsonParametro = array(['mes' => $parametro->getMesAberto(), 'ano' => $parametro->getAnoAberto()]);
            return new JsonResponse($jsonParametro);
        }
        return new JsonResponse('This is not ajax', 505);
    }


}
