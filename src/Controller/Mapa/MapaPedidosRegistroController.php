<?php

namespace App\Controller\Mapa;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class MapaPedidosRegistroController extends AbstractController
{

    /**
     * @Route("/mapa/pedidos/registro", name="mapa_pedidos_registro")
     */
    public function index()
    {

        return $this->render('mapa/lista_pedidos_municao.twig');
    }
}
