<?php

namespace App\Controller\Mapa;

use App\Entity\MapaClienteFornecedor;
use App\Entity\MapaClienteFornecedorEnd;
use App\Entity\MapaMovimento;
use App\Entity\MapaMovimentoArmas;
use App\Entity\MapaProduto;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Doctrine\Persistence\ManagerRegistry;
use function PHPUnit\Framework\isNull;

class MapaMovimentoImportManualController extends AbstractController
{
    private $caracteresEspeciais = array('-', '.', ',', '/');
    private $doctrine;

    public function __construct(HttpClientInterface $tinyClient, ManagerRegistry $doctrine)
    {
        $this->client = $tinyClient;
        $this->doctrine = $doctrine;
    }

    /**
     * @Route("/mapa/movimento/municao/manual", name="mapa_movimento_municao_manual")
     */
    public function getMovimentoImportManual(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('numero', TextType::class, array(
                'required' => true,
                'label' => false
            ))
            ->add('tipoMov', ChoiceType::class, array(
                'choices' => array(
                    'Entrada' => 'E',
                    'Saida' => 'S'
                ),
                'expanded' => true,
                'multiple' => false,
                'required' => true,
                'label' => false,
                'data' => 'S'
            ))
            ->add('tipoPCE', ChoiceType::class, array(
                'choices' => array(
                    'Munição' => 'M',
                    'Arma' => 'A'
                ),
                'expanded' => true,
                'multiple' => false,
                'required' => true,
                'label' => false,
                'attr' => array(
                    'style' => 'margin_left:0'
                ),
                'data' => 'M'
            ))
            ->add('Importar', SubmitType::class, array(
                'attr' => array(
                    'class' => 'btn btn-primary'
                )
            ))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $mov = $form->getData();
            $numeroPed_or_NF = $mov['numero'];
            $tipoMovimento = $mov['tipoMov'];
            $tipoPCE = $mov['tipoPCE'];
            if ($tipoMovimento == 'S' && $tipoPCE == 'M') {
                $this->importMunicaoSaida($numeroPed_or_NF, $tipoMovimento, $tipoPCE);
            }
            if ($tipoMovimento == 'S' && $tipoPCE == 'A') {
                $this->importArmaSaida($numeroPed_or_NF, $tipoMovimento, $tipoPCE);
            }
            if ($tipoMovimento == 'E' && $tipoPCE == 'M') {
                $this->importMunicaoArmaEntrada($numeroPed_or_NF, $tipoMovimento, $tipoPCE);
            }
            if ($tipoMovimento == 'E' && $tipoPCE == 'A') {
                $this->importMunicaoArmaEntrada($numeroPed_or_NF, $tipoMovimento, $tipoPCE);
            }
        }
        return $this->render('mapa/mapa_movimento_import_manual.html.twig', array(
            'form' => $form->createView()
        ));
    }


    private function importMunicaoArmaEntrada($numeroPed_or_NF, $tipoMovimento, $tipoPCE)
    {
        $entityManager = $this->doctrine->getManager();
        $repMapaProduto = $this->doctrine->getRepository(MapaProduto::class);
        $repMapaCliente = $this->doctrine->getRepository(MapaClienteFornecedor::class);
        if ($tipoPCE == 'A') {
            $repMapaMovimento = $this->doctrine->getRepository(MapaMovimentoArmas::class);
        } else {
            $repMapaMovimento = $this->doctrine->getRepository(MapaMovimento::class);
        }
        /**
         * Pesquisa a nota fiscal
         */
        $responseNotas = $this->client
            ->request('GET', '/api2/notas.fiscais.pesquisa.php', [
                'query' => [
                    'numero' => $numeroPed_or_NF,
                    'tipoNota' => 'E'
                ]
            ]);
        $content = $responseNotas->toArray();
        $retorno = $content['retorno'];
        if ($retorno['status'] == 'OK') {
            /**
             * Verifica se apenas uma nota foi retornada
             */
            if (count($retorno['notas_fiscais']) == 1) {
                $cnpj = str_replace($this->caracteresEspeciais, '', $retorno['notas_fiscais'][0]['nota_fiscal']['cliente']['cpf_cnpj']);
                $fornecedor = $repMapaCliente->findOneBy([
                    'cpf' => $cnpj
                ]);
                if (!$fornecedor) {
                    $novoFornecedor = new MapaClienteFornecedor();
                    $novoFornecedor->setCpf(
                        str_replace($this->caracteresEspeciais, '', $retorno['notas_fiscais'][0]['nota_fiscal']['cliente']['cpf_cnpj']));
                    $novoFornecedor->setNome($retorno['notas_fiscais'][0]['nota_fiscal']['cliente']['nome']);
                    $novoFornecedor->setTelefone($retorno['notas_fiscais'][0]['nota_fiscal']['cliente']['fone']);
                    $novoFornecedorEnd = new MapaClienteFornecedorEnd();
                    $novoFornecedorEnd->setEndereco($retorno['notas_fiscais'][0]['nota_fiscal']['cliente']['endereco']);
                    $novoFornecedorEnd->setNumero($retorno['notas_fiscais'][0]['nota_fiscal']['cliente']['numero']);
                    $novoFornecedorEnd->setUf($retorno['notas_fiscais'][0]['nota_fiscal']['cliente']['uf']);
                    $novoFornecedorEnd->setCidade($retorno['notas_fiscais'][0]['nota_fiscal']['cliente']['cidade']);
                    $novoFornecedorEnd->setBairro($retorno['notas_fiscais'][0]['nota_fiscal']['cliente']['bairro']);
                    $novoFornecedorEnd->setCep(
                        str_replace($this->caracteresEspeciais, '', $retorno['notas_fiscais'][0]['nota_fiscal']['cliente']['cep']));
                    $novoFornecedor->setEndereco($novoFornecedorEnd);
                    $entityManager->persist($novoFornecedorEnd);
                    $entityManager->persist($novoFornecedor);
                    $entityManager->flush();
                }
                $mapaMovimento = $repMapaMovimento->findBy([
                    'docFiscal' => $numeroPed_or_NF,
                    'clienteFornecedor' => $fornecedor
                ]);
                /**
                 * Se não existir nenhum movimento passa para importar os produtos
                 */
                if (!$mapaMovimento) {
                    $responseNota = $this->client
                        ->request('GET', '/api2/nota.fiscal.obter.php', [
                            'query' => [
                                'id' => $retorno['notas_fiscais'][0]['nota_fiscal']['id']
                            ]
                        ]);
                    $retornoNotaFiscal = $responseNota->toArray();
                    $notaFiscal = $retornoNotaFiscal['retorno'];
                    if ($notaFiscal['status'] == 'OK') {
                        /**
                         * percorre os itens da nota fiscal, como o código da nota não é exatamente o código do produto
                         * ele faz uma busca pelo produto para retornar o código correto, porém ele traz apenas a qtde
                         * que esta na nota fiscal, que nem sempre é a correta. Como no caso de itens como quilo e gramas
                         */
                        foreach ($notaFiscal['nota_fiscal']['itens'] as $itens) {
                            $responseProduto = $this->client
                                ->request('GET', '/api2/produto.obter.php', [
                                    'query' => [
                                        'id' => $itens['item']['id_produto']
                                    ]
                                ]);
                            $retornoProduto = $responseProduto->toArray();
                            $produtoTiny = $retornoProduto['retorno'];
                            if ($produtoTiny['status'] == 'OK') {
                                $mapaProduto = $repMapaProduto->findOneBy([
                                    'codigo' => $produtoTiny['produto']['codigo']
                                ]);
                                if ($mapaProduto) {
                                    $dataEmissaoNota = \DateTime::createFromFormat(
                                        'd/m/Y',
                                        $notaFiscal['nota_fiscal']['data_emissao']
                                    );
                                    if ($tipoPCE == 'A') {
                                        $novoMovimento = new MapaMovimentoArmas();
                                        $novoMovimento->setSerie(' ');
                                        $novoMovimento->setDataFiscal($dataEmissaoNota);
                                    } else {
                                        $novoMovimento = new MapaMovimento();
                                        $novoMovimento->setRegistro(' ');
                                    }
                                    $novoMovimento->setProduto($mapaProduto);
                                    $novoMovimento->setClienteFornecedor($fornecedor);
                                    $novoMovimento->setQtde($itens['item']['quantidade']);
                                    $novoMovimento->setTipoMov('E');
                                    $novoMovimento->setDocFiscal($notaFiscal['nota_fiscal']['numero']);
                                    $novoMovimento->setDocFiscalItem($itens['item']['id_produto']);
                                    $novoMovimento->setMes($dataEmissaoNota->format('m'));
                                    $novoMovimento->setAno($dataEmissaoNota->format('Y'));
                                    $entityManager->persist($novoMovimento);
                                    $entityManager->flush();
                                    $this->addFlash(
                                        'notice',
                                        'Produto ' . $novoMovimento->getProduto()->getDescricao() . ' [Importado]'
                                    );
                                } else {
                                    $this->addFlash(
                                        'error',
                                        'Produto ' . $itens['item']['descricao'] . ' não encontrado no sistema de Mapa'
                                    );
                                }

                            } else {
                                $this->addFlash(
                                    'error',
                                    'Produto ' . $itens['item']['descricao'] . ' não encontrado no Tiny'
                                );
                            }
                        }
                    }
                }
            } else {
                $this->addFlash(
                    'error',
                    'Consulta retornou mais de um documento com este número'
                );
            }
        } else {
            $this->addFlash(
                'error',
                'Nota não disponível para importação'
            );
        }


    }

    private function importArmaSaida($numeroPed_or_NF, $tipoMovimento, $tipoPCE)
    {
        $entityManager = $this->doctrine->getManager();
        $repMapaProduto = $this->doctrine->getRepository(MapaProduto::class);
        $repMapaCliente = $this->doctrine->getRepository(MapaClienteFornecedor::class);
        $repMapaMovimento = $this->doctrine->getRepository(MapaMovimentoArmas::class);
        $mapaMovimento = $repMapaMovimento->findBy([
            'docFiscal' => $numeroPed_or_NF
        ]);
        if ($mapaMovimento) {
            $this->addFlash(
                'notice',
                'Este movimento já foi cadastrado'
            );

        } else {
            $responseNotas = $this->client
                ->request('GET', '/api2/notas.fiscais.pesquisa.php', [
                    'query' => [
                        'numero' => $numeroPed_or_NF,
                        'tipoNota' => 'S',
                    ]
                ]);
            $content = $responseNotas->toArray();
            $retorno = $content['retorno'];
            if ($retorno['status'] == 'OK') {
                if (count($retorno['notas_fiscais']) == 1) {
                    $idNota = $retorno['notas_fiscais'][0]['nota_fiscal']['id'];
                } else {
                    foreach ($retorno['notas_fiscais'] as $notas) {
                        if ($notas['nota_fiscal']['nome'] <> 'Consumidor Final') {
                            $idNota = $notas['nota_fiscal']['id'];
                        }
                    }
                }

                $responseNota = $this->client
                    ->request('GET', '/api2/nota.fiscal.obter.php', [
                        'query' => [
                            'id' => $idNota,
                        ]
                    ]);
                $conteudoNota = $responseNota->toArray();
                $notaTiny = $conteudoNota['retorno'];
                if ($notaTiny['status'] == 'OK') {
                    foreach ($notaTiny['nota_fiscal']['itens'] as $itens) {
                        $mapaProduto = $repMapaProduto->findOneBy([
                                'codigo' => $itens['item']['codigo']
                            ]
                        );
                        if ($mapaProduto) {
                            $mapaMovimento = new MapaMovimentoArmas();
                            $mapaMovimento->setProduto($mapaProduto);
                            $mapaMovimento->setSerie('REGISTRO PENDENTE');
                            $mapaMovimento->setDocFiscal($notaTiny['nota_fiscal']['numero']);
                            $mapaMovimento->setDocFiscalItem($itens['item']['id_produto']);
                            $mapaMovimento->setTipoMov('S');
                            $mapaMovimento->setQtde($itens['item']['quantidade']);
                            $dataEmissaoNota = \DateTime::createFromFormat(
                                'd/m/Y',
                                $notaTiny['nota_fiscal']['data_emissao']
                            );
                            /**
                             * Mês e ano não são preenchidos na importação, pois assim o produto permanece como
                             * pendente.
                             */
//                            $mapaMovimento->setMes($dataEmissaoNota->format('m'));
//                            $mapaMovimento->setAno($dataEmissaoNota->format('Y'));
                            $mapaMovimento->setDataFiscal($dataEmissaoNota);
                            $mapaCliente = $repMapaCliente->findOneBy([
                                'cpf' => str_replace($this->caracteresEspeciais, '', $notaTiny['nota_fiscal']['cliente']['cpf_cnpj'])
                            ]);
                            if ($mapaCliente) {
                                /**
                                 * TODO: verificar se o cliente existe e se os dados estão atualizados
                                 */
                                $mapaMovimento->setClienteFornecedor($mapaCliente);
                            } else {
                                $mapaCliente = new MapaClienteFornecedor();
                                $mapaCliente->setNome($notaTiny['nota_fiscal']['cliente']['nome']);
                                $mapaCliente->setTelefone($notaTiny['nota_fiscal']['cliente']['fone']);
                                $mapaCliente->setCpf(
                                    str_replace($this->caracteresEspeciais, '', $notaTiny['nota_fiscal']['cliente']['cpf_cnpj'])
                                );
                                $mapaClienteEndereco = new MapaClienteFornecedorEnd();
                                $mapaClienteEndereco->setClienteFornecedor($mapaCliente);
                                $mapaClienteEndereco->setCep(
                                    str_replace($this->caracteresEspeciais, '', $notaTiny['nota_fiscal']['cliente']['cep'])
                                );
                                $mapaClienteEndereco->setBairro($notaTiny['nota_fiscal']['cliente']['bairro']);
                                $mapaClienteEndereco->setCidade($notaTiny['nota_fiscal']['cliente']['cidade']);
                                $mapaClienteEndereco->setUf($notaTiny['nota_fiscal']['cliente']['uf']);
                                $mapaClienteEndereco->setEndereco($notaTiny['nota_fiscal']['cliente']['endereco']);
                                $mapaClienteEndereco->setComplemento($notaTiny['nota_fiscal']['cliente']['complemento']);
                                $mapaCliente->setEndereco($mapaClienteEndereco);
                                $mapaMovimento->setClienteFornecedor($mapaCliente);
                            }
                            $entityManager->persist($mapaMovimento);
                            $entityManager->flush();
                            $this->addFlash(
                                'notice',
                                'Cliente: ' . $mapaCliente->getNome() . ' - ' .
                                $mapaMovimento->getProduto()->getDescricao() . ' cadastrado com sucesso'
                            );
                        }
                    }
                }
            }

        }
    }

    private function importMunicaoSaida($numeroPed_or_NF, $tipoMovimento, $tipoPCE)
    {
        $entityManager = $this->doctrine->getManager();
        $repMapaProduto = $this->doctrine->getRepository(MapaProduto::class);
        $repMapaCliente = $this->doctrine->getRepository(MapaClienteFornecedor::class);
        $repMapaMovimento = $this->doctrine->getRepository(MapaMovimento::class);
        $mapaMovimento = $repMapaMovimento->findBy([
            'docFiscal' => 'P' . $numeroPed_or_NF
        ]);
        if ($mapaMovimento) {
            $this->addFlash(
                'notice',
                'Este movimento já foi cadastrado'
            );

        } else {
            /*ENCONTRA O ID DO PEDIDO PELO SEU NUMERO*/
            $responsePedidos = $this->client
                ->request('GET', '/api2/pedidos.pesquisa.php', [
                    'query' => [
                        'numero' => $numeroPed_or_NF,
                    ]
                ]);
            $content = $responsePedidos->toArray();
            $retorno = $content['retorno'];
            if ($retorno['status'] == 'OK') {
                $idPedido = $retorno["pedidos"][0]["pedido"]["id"];
                /* ENCONTRA PEDIDO PELO SEU ID*/
                $responsePedido = $this->client
                    ->request('GET', '/api2/pedido.obter.php', [
                        'query' => [
                            'id' => $idPedido,
                        ]
                    ]);
                $conteudoNota = $responsePedido->toArray();
                $pedidoTiny = $conteudoNota['retorno'];
                if ($pedidoTiny['status'] == 'OK') {
                    foreach ($pedidoTiny['pedido']['itens'] as $itens) {
                        $mapaProduto = $repMapaProduto->findOneBy([
                                'codigo' => $itens['item']['codigo']
                            ]
                        );
                        if ($mapaProduto) {
                            $mapaMovimento = new MapaMovimento();
                            $mapaMovimento->setProduto($mapaProduto);
                            $mapaMovimento->setRegistro($pedidoTiny['pedido']['obs_interna']);
                            $mapaMovimento->setDocFiscal('P' . $pedidoTiny['pedido']['numero']);
                            $mapaMovimento->setDocFiscalItem($itens['item']['id_produto']);
                            $mapaMovimento->setTipoMov('S');
                            $mapaMovimento->setQtde($itens['item']['quantidade']);
                            $dataEmissaoPedido = \DateTime::createFromFormat(
                                'd/m/Y',
                                $pedidoTiny['pedido']['data_faturamento']
                            );
                            $mapaMovimento->setMes($dataEmissaoPedido->format('m'));
                            $mapaMovimento->setAno($dataEmissaoPedido->format('Y'));
                            $mapaCliente = $repMapaCliente->findOneBy([
                                'cpf' => str_replace($this->caracteresEspeciais, '', $pedidoTiny['pedido']['cliente']['cpf_cnpj'])
                            ]);
                            if ($mapaCliente) {
                                /**
                                 * TODO: verificar se o cliente existe e se os dados estão atualizados
                                 */
                                $mapaMovimento->setClienteFornecedor($mapaCliente);
                            } else {
                                if ($pedidoTiny['pedido']['cliente']['nome'] == 'Consumidor Final') {
                                    $mapaCliente = $repMapaCliente->findOneBy([
                                        'nome' => 'Consumidor Final'
                                    ]);
                                    $mapaMovimento->setClienteFornecedor($mapaCliente);
                                } else {
                                    $mapaCliente = new MapaClienteFornecedor();
                                    $mapaCliente->setNome($pedidoTiny['pedido']['cliente']['nome']);
                                    $mapaCliente->setTelefone($pedidoTiny['pedido']['cliente']['fone']);
                                    $mapaCliente->setCpf(
                                        str_replace($this->caracteresEspeciais, '', $pedidoTiny['pedido']['cliente']['cpf_cnpj'])
                                    );
                                    $mapaClienteEndereco = new MapaClienteFornecedorEnd();
                                    $mapaClienteEndereco->setClienteFornecedor($mapaCliente);
                                    $mapaClienteEndereco->setCep(
                                        str_replace($this->caracteresEspeciais, '', $pedidoTiny['pedido']['cliente']['cep'])
                                    );
                                    $mapaClienteEndereco->setBairro($pedidoTiny['pedido']['cliente']['bairro']);
                                    $mapaClienteEndereco->setCidade($pedidoTiny['pedido']['cliente']['cidade']);
                                    $mapaClienteEndereco->setUf($pedidoTiny['pedido']['cliente']['uf']);
                                    $mapaClienteEndereco->getEndereco($pedidoTiny['pedido']['cliente']['endereco']);
                                    $mapaClienteEndereco->setComplemento($pedidoTiny['pedido']['cliente']['complemento']);
                                    $mapaMovimento->setClienteFornecedor($mapaCliente);
                                }
                            }
                            $entityManager->persist($mapaMovimento);
                            $entityManager->flush();
                            $this->addFlash(
                                'notice',
                                $mapaMovimento->getProduto()->getDescricao() . ' cadastrado com sucesso'
                            );

                        }
                    }
                }
            } else {
                $this->addFlash(
                    'error',
                    $retorno['erros'][0]['erro']
                );
            }

        }
    }
}
