<?php

namespace App\Controller\Mapa;

use App\Entity\MapaCalibre;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class MapaCalibreController extends AbstractController
{
    private $doctrine;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }


    /**
     * @Route("/mapa/calibre", name="mapa_calibre")
     */
    public function index()
    {
        $repository = $this->doctrine->getRepository(MapaCalibre::class);
        $calibres = $repository->findAll();

        return $this->render('mapa/lista_mapa_calibre.twig', [
            'controller_name' => 'MapaCalibreController', 'calibres' => $calibres
        ]);
    }
}
