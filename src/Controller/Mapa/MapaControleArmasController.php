<?php

namespace App\Controller\Mapa;

use App\Entity\MapaEstoque;
use App\Entity\MapaMovimento;
use App\Entity\MapaMovimentoArmas;
use App\Entity\MapaParametro;
use App\Entity\ParametrosMapa;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class MapaControleArmasController extends AbstractController
{

    private $doctrine;

    public function __construct(HttpClientInterface $tinyClient, ManagerRegistry $doctrine)
    {
        $this->client = $tinyClient;
        $this->doctrine = $doctrine;
    }


    /**
     * @Route("/mapa/controle/armas", name="mapa_controle_armas")
     */
    public function controleArmas()
    {
        $repository = $this->doctrine->getRepository(ParametrosMapa::class);
        $parametro = $repository->find(1);
        return $this->render('mapa/mapa_controle_armas.twig', [
            'parametro' => $parametro
        ]);
    }


    /**
     * @Route("/mapa/controle/armas/pendentes", name="mapa_armas_pendentes")
     */
    public function armasPendentes()
    {

        $repository = $this->doctrine->getRepository(ParametrosMapa::class);
        $parametro = $repository->find(1);
        return $this->render('mapa/mapa_armas_pendentes.twig', [
            'parametro' => $parametro
        ]);
    }

    /**
     * @Route ("/mapa/movimento/arma",name="mapa_movimento_id")
     */
    public function atualizarMesAnoMovimento(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $id = $request->request->get('id');
            $data = explode('/', $request->request->get('data'));
            if ($data[0] >= 1 && $data[0 <= 12]) {
                if ($data[1] >= 1900 && $data[1] <= date('Y')) {

                    $repMovimentoArmas = $this->doctrine->getRepository(MapaMovimentoArmas::class);
                    $mov = $repMovimentoArmas->find($id);
                    if ($mov) {
                        $mov->setMes($data[0]);
                        $mov->setAno($data[1]);
                        $entityManager = $this->doctrine->getManager();
                        $entityManager->persist($mov);
                        $entityManager->flush();
                        return new JsonResponse('ok', 200);
                    }
                    return new JsonResponse('Record not found', 404);
                }
            }
            return new JsonResponse('Data incorreta', 405);
        }
        return new JsonResponse('Is not Ajax', 405);
    }

    /**
     * @Route ("/mapa/movimento/armas/pendentes", name="movimento_armas_pendentes")
     */
    public function movimentosArmasPendentes(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $repMovimentoArmas = $this->doctrine->getRepository(MapaMovimentoArmas::class);
            $armasPendentes = $repMovimentoArmas->findBy([
                'mes' => null,
                'ano' => null
            ]);
            if ($armasPendentes) {
                $armasPendentesResponse = array();
                foreach ($armasPendentes as $mov) {
                    array_push($armasPendentesResponse, array(
                        "id" => $mov->getId(),
                        "ano" => $mov->getAno(),
                        "mes" => $mov->getMes(),
                        "cliente" => $mov->getClienteFornecedor()->getNome(),
                        "data" => date_format($mov->getDataFiscal(), 'd/m/Y'),
                        "produto" => $mov->getProduto()->getDescricao(),
                        "serie" => $mov->getSerie(),
                        "qtde" => $mov->getQtde()
                    ));
                }

                return new JsonResponse($armasPendentesResponse);
            }
            return new JsonResponse('not found', 404);

        }
        return new JsonResponse('Is not Ajax', 405);
    }

    /**
     * @Route("/mapa/estoque/armas/saldos", name="mapa_estoque_armas_saldos",methods={"GET"})
     */
    public function retornaIventarioArmas(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $repMapaEstoque = $this->doctrine->getRepository(MapaEstoque::class);
            $repParamentro = $this->doctrine->getRepository(ParametrosMapa::class);
            $parametro = $repParamentro->find(1);
            $tipoCalibre = array('Arma');
            $mapaEstoques = $repMapaEstoque->findByTipoCalibre(
                $parametro->getAnoAbertovendaArma(),
                $parametro->getMesAbertoVendaArma(),
                $tipoCalibre
            );
            if ($mapaEstoques) {
                $estoqueArray = array();
                foreach ($mapaEstoques as $estoque) {
                    array_push($estoqueArray, array(
                        "id" => $estoque->getId(),
                        "calibre" => $estoque->getCalibre()->getDescricao(),
                        "tipo" => $estoque->getCalibre()->getTipoCalibre(),
                        "saldoInicial" => $estoque->getQtdeInicial(),
                        "entrada" => $estoque->getEntrada(),
                        "saida" => $estoque->getSaida(),
                        "saldoFinal" => $estoque->getQtdeFinal()
                    ));
                }
                return new JsonResponse($estoqueArray);
            }
            return new JsonResponse('not found', 404);
        }
        return new JsonResponse('Is not Ajax', 405);
    }

    /**
     * @Route("/mapa/armas/saldos/calcular", name="mapa_armas_saldos_calcular",methods={"GET"})
     */
    public function calcularSaldosArmas(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $repParametro = $this->doctrine->getRepository(ParametrosMapa::class);
            $repMapaEstoque = $this->doctrine->getRepository(MapaEstoque::class);
            $entityManager = $this->doctrine->getManager();
            $repMapaMovimento = $this->doctrine->getRepository(MapaMovimentoArmas::class);
            $parametro = $repParametro->find(1);
            $tipoCalibre = array('Arma');

            if ($parametro->getMesAbertoVendaArma() == 1) {
                $mesUltimoSaldo = 12;
                $anoUltimoSaldo = $parametro->getAnoAbertovendaArma() - 1;
            } else {
                $mesUltimoSaldo = $parametro->getMesAbertoVendaArma() - 1;
                $anoUltimoSaldo = $parametro->getAnoAbertovendaArma();
            }
            $repMapaEstoque->deleteMonthYear(
                $parametro->getMesAbertoVendaArma(),
                $parametro->getAnoAbertovendaArma(),
                $tipoCalibre
            );

            $ultimoEstoqueArray = $repMapaEstoque->findByTipoCalibre(
                $anoUltimoSaldo,
                $mesUltimoSaldo,
                $tipoCalibre
            );

            if (!$ultimoEstoqueArray) {
                return new JsonResponse('Ultimo inventario não encontrado', 404);
            }
            foreach ($ultimoEstoqueArray as $estoque) {
                $novoEstoque = new MapaEstoque();
                $novoEstoque->setMes($parametro->getMesAbertoVendaArma());
                $novoEstoque->setAno($parametro->getAnoAbertovendaArma());
                $novoEstoque->setCalibre($estoque->getCalibre());
                $novoEstoque->setEntrada(0);
                $novoEstoque->setQtdeFinal(0);
                $novoEstoque->setSaida(0);
                $novoEstoque->setQtdeInicial($estoque->getQtdeFinal());
                $entityManager->persist($novoEstoque);
            }
            $entityManager->flush();

            $movimentos = $repMapaMovimento->findBy([
                'mes' => $parametro->getMesAbertoVendaArma(),
                'ano' => $parametro->getAnoAbertovendaArma()
            ]);

//            if (!$movimentos) {
//                return new JsonResponse('movimento not found', 404);
//            }
            foreach ($movimentos as $mov) {
                $est = $repMapaEstoque->findOneBy([
                    'mes' => $parametro->getMesAbertoVendaArma(),
                    'ano' => $parametro->getAnoAbertovendaArma(),
                    'calibre' => $mov->getProduto()->getCalibre()
                ]);
//              *****  Se encontrar estoque soma entrada/saida
                if ($est) {
                    if ($mov->getTipoMov() == 'E') {
                        $est->setEntrada($est->getEntrada() + $mov->getQtde());
                    } else {
                        $est->setSaida($est->getSaida() + $mov->getQtde());
                    }
                    $entityManager->persist($est);

                } else {
//               *********  Se não encontrar cria novo estoque (nova saldo de arma)
                    $novoEstoque = new MapaEstoque();
                    $novoEstoque->setQtdeInicial(0);
                    $novoEstoque->setQtdeFinal(0);
                    if ($mov->getTipoMov()=='E'){
                        $novoEstoque->setEntrada($mov->getQtde());
                        $novoEstoque->setSaida(0);
                    }else{
                        $novoEstoque->setEntrada(0);
                        $novoEstoque->setSaida($mov->getQtde());
                    }
                    $novoEstoque->setCalibre($mov->getProduto()->getCalibre());
                    $novoEstoque->setAno($parametro->getAnoAbertovendaArma());
                    $novoEstoque->setMes($parametro->getMesAbertoVendaArma());
                    $entityManager->persist($novoEstoque);
                    $entityManager->flush();
                }
                $entityManager->flush();
            }

            //****** Calcula Saldo final para o mes

            $estoques = $repMapaEstoque->findByTipoCalibre(
                $parametro->getAnoAbertovendaArma(),
                $parametro->getMesAbertoVendaArma(),
                $tipoCalibre
            );
            if (!$estoques) {
                return new JsonResponse('estoques não encontrado [saldo final]', 404);
            }

            foreach ($estoques as $est) {
                $est->setQtdeFinal($est->getQtdeInicial() + $est->getEntrada() - $est->getSaida());
                $entityManager->persist($est);

            }
            $parametro->setUltSaldoArma(\DateTime::createFromFormat('Y-m-d', date('Y-m-d')));
            $entityManager->persist($parametro);
            $entityManager->flush();
            return new JsonResponse('Saldo calculado', 200);

        }
        return new JsonResponse('This is not ajax', 505);
    }

    /**
     * @Route("/mapa/armas/fechar/mes", name="mapa_armas_fechar_mes",methods={"GET"})
     */
    public function fecharMesArmas(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $repParamento = $this->doctrine->getRepository(ParametrosMapa::class);
            $parametro = $repParamento->find(1);

            if ($parametro->getMesAbertoVendaArma() == 12) {
                $parametro->setMesAbertoVendaArma(1);
                $parametro->setAnoAbertovendaArma($parametro->getAnoAbertovendaArma() + 1);
            } else {
                $parametro->setMesAbertoVendaArma($parametro->getMesAbertoVendaArma() + 1);
            }

            $entitymanager = $this->doctrine->getManager();
            $entitymanager->persist($parametro);
            $entitymanager->flush();
            return new JsonResponse('ok', 200);

        }
        return new Response('This is not ajax', 505);
    }

    /**
     * @Route("/mapa/armas/reabrir/mes", name="mapa_armas_reabrir_mes",methods={"GET"})
     */
    public function reabrirMesArmas(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $repParamento = $this->doctrine->getRepository(ParametrosMapa::class);
            $parametro = $repParamento->find(1);

            if ($parametro->getMesAbertoVendaArma() == 1) {
                $parametro->setMesAbertoVendaArma(12);
                $parametro->setAnoAbertovendaArma($parametro->getAnoAbertovendaArma() - 1);
            } else {
                $parametro->setMesAbertoVendaArma($parametro->getMesAbertoVendaArma() - 1);
            }

            $entitymanager = $this->doctrine->getManager();
            $entitymanager->persist($parametro);
            $entitymanager->flush();

            return new JsonResponse('ok', 200);

        }
        return new JsonResponse('This is not ajax', 505);
    }

    /**
     * @Route("/mapa/parametros/armas", name="mapa_parametros_armas",methods={"GET"})
     */

    public function getParamArmas(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $repParametro = $this->doctrine->getRepository(ParametrosMapa::class);
            $parametro = $repParametro->find(1);

            $jsonParametro = array(['mes' => $parametro->getMesAbertoVendaArma(), 'ano' => $parametro->getAnoAbertovendaArma()]);
            return new JsonResponse($jsonParametro);
        }
        return new JsonResponse('This is not ajax', 505);
    }


}
