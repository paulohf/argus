<?php

namespace App\Controller;

use App\Entity\MapaClienteFornecedor;
use App\Entity\MapaProduto;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Produto;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class APIController extends AbstractController
{

    private $host = 'localhost:C:\\Forti Up\\DADOS\\AOPESCADOR.FDB';
    private $username = 'SYSDBA';
    private $password = 'masterkey';
    private $charset = 'ISO8859_9';

    public function __construct(HttpClientInterface $tinyClient)
    {
        $this->client = $tinyClient;
    }

    /**
     * @Route("/request/tiny/produto", name="request_tiny_produto",methods={"POST"})
     */
    public function getProdutoEtiquetaTiny(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $produtoRequest = "";
            $produtoRequest = $request->request->get('codigo');
            if ($produtoRequest <> "") {

                $response = $this->client
                    ->request('GET', '/api2/produtos.pesquisa.php', [
                        'query' => [
                            'pesquisa' => $produtoRequest,
                        ]
                    ]);
                $respListaProd = $response->toArray();
                if ($respListaProd['retorno']['status'] == 'OK') {
                    $id = null;
                    foreach ($respListaProd['retorno']['produtos'] as $produtos) {
                        if ($produtos['produto']['codigo'] == $produtoRequest) {
                            $id = $produtos['produto']['id'];
                        }
                    }
                    if ($id == null) {
                        return new Response('Não foram encontrados registros para a consulta', 404);
                    }
                    $response = $this->client
                        ->request('GET', '/api2/produto.obter.php', [
                            'query' => [
                                'id' => $id
                            ]
                        ]);
                    $produto = $response->toArray();
                    $preco = 0;
                    if ($produto['retorno']['status'] == 'OK') {
                        if($produto['retorno']['produto']['tipoVariacao']=='P'){
                            return new Response('Produto com variações: Escolher uma variação do produto', 406);
                        }
                        if($produto['retorno']['produto']['preco_promocional']>0){
                            $preco = $produto['retorno']['produto']['preco_promocional'];
                        }else{
                            $preco = $produto['retorno']['produto']['preco'];
                        }
                        return new JsonResponse(array(
                            'codigo' => $produto['retorno']['produto']['codigo'],
                            'descricao' => $produto['retorno']['produto']['nome'],
                            'preco' => $preco,
                            'descEtiqueta' => $produto['retorno']['produto']['nome'],
                            'idproduto' => $produto['retorno']['produto']['id'],
                            'avista' => $preco,
                            'parc2' => $preco,
                            'parc3' => $preco

                        ));
                    }
                }
                return new Response('Não foram encontrados registros para a consulta', 404);
            }
        }
        return new Response('This is not ajax!', 400);
    }


    /**
     * @Route("/request/tiny/produto/descricao", name="tiny_produto_descricao")
     *
     */
    public function getDescricaoProdutoTiny(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $descRequest = $request->request->get('term');
            $response = $this->client
                ->request('GET', '/api2/produtos.pesquisa.php', [
                    'query' => [
                        'pesquisa' => $descRequest,
                    ]
                ]);
            $content = $response->toArray();
            $retorno = $content['retorno'];
            if ($retorno['status'] == 'OK') {
                $jsonProdutos = array();
                foreach ($retorno['produtos'] as $produto) {
                    array_push($jsonProdutos, array('value' => $produto['produto']['codigo'],
                        'label' => $produto['produto']['nome']));
                }
                return new JsonResponse($jsonProdutos);
            }
            return new Response('Não foram encontrados registros para a consulta', 404);
        }
        return new Response('This is not ajax!', 400);
    }

    /**
     * @Route("/request/mapa/cliente", name="request_mapa_cliente")
     *
     */
    function getMapaCliente(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $data = '%' . strtoupper($request->query->get('nome')) . '%';
            $repository = $this->getDoctrine()->getRepository(MapaClienteFornecedor::class);
            $clientes = $repository->findByName($data);
//            $clientes  =$repository->findAll();


            if ($clientes) {
                $clientesArray = array();
                foreach ($clientes as $cli) {
                    array_push($clientesArray, array(
                        "value" => $cli->getId(),
                        "label" => $cli->getNome(),
                    ));
                }

                return new JsonResponse(($clientesArray));
            }
            return new Response('Record not found', 404);
        }
        return new Response('This is not XmlHttpRequest', 405);
    }

    /**
     * @Route("/request/mapa/produto", name="request_mapa_produto")
     *
     */

    public function getProduto(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $data = $request->query->get('codigo');
            $repository = $this->getDoctrine()->getRepository(MapaProduto::class);
            $produto = $repository->findOneBy([
                'codigo' => $data
            ]);
            if ($produto) {
                $produtoArray = array();
                array_push($produtoArray, array(
                    "value" => $produto->getId(),
                    "label" => $produto->getDescricao(),
                ));
                return new JsonResponse(($produtoArray));
            }
            return new Response('Record not found', 404);
        }
        return new Response('This is not XmlHttpRequest', 400);
    }

}
