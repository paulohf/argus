<?php

namespace App\Form;

use App\Entity\Etiqueta;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EtiquetaVaraNFType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idProduto',IntegerType::class)
            ->add('codigo',TextType::class)
            ->add('descricao', TextType::class)
            ->add('preco',TextType::class)
            ->add('qtde', IntegerType::class);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => Etiqueta::class,));
    }

    public function getName()
    {
        return 'app_bundle_etiqueta_vara_nf_type';
    }
}
