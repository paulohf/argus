<?php

namespace App\Form;

use App\Entity\NotaFiscalMapa;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NotaFiscalArmaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nome', TextType::class, array('label' => false))
            ->add('cpf', TextType::class, array('label' => false))
            ->add('produto', TextType::class, array('label' => false))
            ->add('data', DateType::class, array('label' => false , 'widget'=>'single_text'))
            ->add('fabricante', TextType::class, array('label' => false, 'required' => false))
            ->add('endereco',TextType::class,array('label'=>false))
            ->add('notafiscal', TextType::class,array('label'=>false))
            ->add('serial', TextType::class, array('label' => false, 'required' => false));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => NotaFiscalMapa::class));
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_nota_fiscal_arma_type';
    }
}
