<?php

namespace App\Form;

use App\Entity\OrdemServico;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrdemServicoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("nome",TextType::class)
            ->add("telefone",TextType::class)
            ->add("produto", TextType::class)
            ->add('urgente',CheckboxType::class,['required'=>false])
            ->add("orcamento",CheckboxType::class,['required'=>false])
            ->add("problema", TextareaType::class, array(
                'attr' => array('style' => 'width:100%'))
            );

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => OrdemServico::class));

    }

    public function getBlockPrefix()
    {
        return 'app_bundle_ordem_servico_type';
    }
}
